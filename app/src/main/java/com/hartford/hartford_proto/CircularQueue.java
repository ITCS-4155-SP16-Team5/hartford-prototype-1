package com.hartford.hartford_proto;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Jacob on 4/6/2016.
 */
public class CircularQueue {
    private int queueSize;
    private ArrayList<Double> array;
    private int index;
    public CircularQueue(int queueSize) {
        this.queueSize = queueSize;
        array = new ArrayList<>();
        index = 0;
    }

    public void add(double item) {
        if (array.size() < queueSize) {
            array.add(item);

        } else {
            array.set(index,item);
            moveIndex();


        }


    }

    private void moveIndex() {
        //Log.d("QUEUE", "Queue size: " + queueSize + " Index: " + index);
        if(index + 1 < queueSize ) {
            index++;

        } else {
            index = 0;
        }
    }

    public double getAverage() {
        double avg = 0;

        for (int i = 0; i < array.size(); i++) {
            //Log.d("QUEUE", "queue loop");
            avg += array.get(i);
        }

        return avg/array.size();
    }

}
