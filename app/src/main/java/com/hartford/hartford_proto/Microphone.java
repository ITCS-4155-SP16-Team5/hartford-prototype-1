package com.hartford.hartford_proto;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import java.text.DecimalFormat;

public class Microphone {

    private final MainActivity activity;
    private final boolean useBluetooth;
    private AudioRecord audio;
    private int bufferSize;
    private double lastLevel = 0;
    private Thread thread;
    private static final int SAMPLE_DELAY = 250;
    DecimalFormat df;
    CircularQueue cq;
    AudioManager am;
    int totalRecords;
    double dailyAvg = 0;
    double sumLevels = 0;
    double dailyPeak = 0;
    double offset = 0, multiplier = 1;
    private static int[] mSampleRates = new int[]{44100, 22050, 11025, 8000};

    public Microphone(MainActivity activity, boolean useBluetooth) {
        this.useBluetooth = useBluetooth;
        this.activity = activity;
        cq = new CircularQueue(4);
        df = new DecimalFormat("###.##");

        try {
            setup();
        } catch (Exception e) {
            android.util.Log.e("TrackingFlow", "Exception", e);
        }

    }

    public String getMeasurement() {
        if (!Double.isInfinite(lastLevel)) {
            return df.format(Math.abs(lastLevel));
        }
        return "0";
    }

    public void setup() {
        if (useBluetooth) {
            am = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
            am.startBluetoothSco();
        }

        audio = findAudioRecord();
        audio.startRecording();
        thread = new Thread(new Runnable() {
            public void run() {
                //Log.d("MIC", "Before while loop");
                while (thread != null && !thread.isInterrupted()) {
                    //Log.d("MIC", "In deviceMic thread");
                    //Let's make the thread sleep for a the approximate sampling time
                    try {
                        Thread.sleep(SAMPLE_DELAY);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                    readAudioBuffer();//After this call we can get the last value assigned to the lastLevel variable
                }
            }
        });
        thread.start();
        activity.bluetoothScoOn(am.isBluetoothScoOn());
    }

    /**
     * Functionality that gets the sound level out of the sample
     */
    public String readAudioBuffer() {

        try {
            short[] buffer = new short[bufferSize / 2];


            if (audio != null) {

                // Sense the sound
                int bufferReadResult = audio.read(buffer, 0, bufferSize / 2);
                double sumLevel = 0;
                for (int i = 0; i < bufferReadResult; i++) {
                    double y = buffer[i] / 32768.0;
                    sumLevel += y * y;
                }
                double rms = Math.sqrt(sumLevel / bufferReadResult);

                //Convert to dB
                double Db = 20.0 * Math.log10(rms);
                //Log.d("QUEUE", "Raw value: " + Db);
                Db = Math.abs(Db);

//                if (Db > 50) {
//                    //scales the difference below the 50 dB threshold, where accuracy begins to fall with our device
//                    Db = Db - ((5 * (50 - Db) / 10));
//
//                }

                //use a circular queue to average out the readings over time.
                cq.add(100 - Math.abs(Db));
                lastLevel = cq.getAverage();
                calculateMetrics(lastLevel);
                //lastLevel = Db;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return df.format(lastLevel);
    }

    private void calculateMetrics(double recording) {
        boolean newPeak = false;

        if (recording > dailyPeak) {
            dailyPeak = recording;
            newPeak = true;
        }

        totalRecords++;
        sumLevels += recording;
        dailyAvg = sumLevels / totalRecords;

        activity.setAverageAndPeak(dailyAvg, dailyPeak, newPeak);

    }

    public AudioRecord findAudioRecord() {
        for (int rate : mSampleRates) {
            for (short audioFormat : new short[]{AudioFormat.ENCODING_PCM_16BIT, AudioFormat.ENCODING_PCM_8BIT}) {
                for (short channelConfig : new short[]{AudioFormat.CHANNEL_IN_STEREO, AudioFormat.CHANNEL_IN_MONO}) {
                    try {
                        Log.d("RECORD", "Attempting rate " + rate + "Hz, bits: " + audioFormat + ", channel: "
                                + channelConfig);
                        bufferSize = AudioRecord.getMinBufferSize(rate, channelConfig, audioFormat);

                        if (bufferSize != AudioRecord.ERROR_BAD_VALUE) {
                            // check if we can instantiate and have a success
                            AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, rate, channelConfig, audioFormat, bufferSize);

                            if (recorder.getState() == AudioRecord.STATE_INITIALIZED)
                                return recorder;
                        }
                    } catch (Exception e) {
                        Log.e("RECORD", rate + "Exception, keep trying.", e);
                    }
                }
            }
        }
        return null;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }
}