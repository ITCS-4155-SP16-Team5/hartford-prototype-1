package com.hartford.hartford_proto;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements DeviceListFragment.OnFragmentInteractionListener, DashboardFragment.OnFragmentInteractionListener{

    private static final long MIN_NOTIFICATION_INTERVAL = 1800000;
    private static String USERNAME = "John Doe";
    final private int  transmissionInterval = 5050;
    final private int recordInterval = 1000;
    public DeviceItem currentBluetoothDevice;
    final private int YELLOW_VALUE = 70;
    final private int RED_VALUE = 90;
    private boolean notificationSignaled;
    public BluetoothAdapter BTAdapter;
    long nextNotificationTime;
    public ArrayList<DeviceItem> deviceItemList;
    private DeviceListFragment mDeviceListFragment;
    private DashboardFragment mDashboardFragment;
    private BluetoothConnection bConnection;
    private TextView ambientTV, headphoneTV;
    private DeviceItem defaultDevice;
    public Microphone deviceMic;
    Thread updateThread;
    private double dailyAvg;
    private double dailyPeak;
    private boolean bluetoothActive;
    public double workLocationx = 0;
    public double workLocationy = 0;
    public double workSquare = .006;
    private boolean isAppDisabled = false;
    DecimalFormat df;
//    private int offset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        ambientTV = (TextView) findViewById(R.id.ambientDBText);
        headphoneTV = (TextView) findViewById(R.id.headphoneDBText);

        //Disabled for now; attempting to use built-in bluetooth functions from a headset
        //setupBluetooth();
        setupWithoutBluetooth();
        df = new DecimalFormat("###.##");
    }

    public void setupWithoutBluetooth() {
        openDash();
        setupClasses(null);
    }

//    public void setupBluetooth() {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
//        BTAdapter = BluetoothAdapter.getDefaultAdapter();
//        if (!BTAdapter.isEnabled()) {
//            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableBT, 1);
//
//        }
//        String name = preferences.getString("BLUETOOTH_DEFAULT", "");
//        Log.d("NAME", name);
//        if(name.equalsIgnoreCase(""))
//        {
//            FragmentManager fragmentManager = getSupportFragmentManager();
//
//            mDeviceListFragment = DeviceListFragment.newInstance(BTAdapter);
//            fragmentManager.beginTransaction().replace(R.id.container, mDeviceListFragment).commit();
//        } else {
//            Log.d("NAME", "IN ELSE STATEMENT");
//
//            Set<BluetoothDevice> pairedDevices = BTAdapter.getBondedDevices();
//            if (pairedDevices.size() > 0) {
//                for (BluetoothDevice device : pairedDevices) {
//                    if(device.getName().equals(name)) {
//                        defaultDevice = new DeviceItem(device.getName(), device.getAddress(), "false", device);
//                        onFragmentInteraction(defaultDevice);
//                        break;
//                    }
//
//                }
//            }
//        }
//
//

    public void setupClasses (BluetoothConnection bt) {
        deviceMic = new Microphone(this, true);
        nextNotificationTime = System.currentTimeMillis();
        notificationSignaled = false;
        //class declarations
        //BluetoothReceiver bt = new BluetoothReceiver();
        GeoLocator geo = new GeoLocator(this, this);
        HTTPSender http = new HTTPSender();
        SQLiteHandler db = new SQLiteHandler(this, bt, http, geo, deviceMic);
        http.setDB(db);
        DataTransmissionThread dt = new DataTransmissionThread(this, db, http, transmissionInterval);
        dt.start();
        DataCollectionThread dc = new DataCollectionThread(recordInterval, db, geo, this);
        dc.start();
       updateThread = new Thread(new Runnable() {
            public void run() {
                while(updateThread!= null && !updateThread.isInterrupted()){

                    //Make the thread sleep for the approximate sampling time
                    try{Thread.sleep(100);}catch(InterruptedException ie){ie.printStackTrace();}

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pushDashboardUpdate(deviceMic.getMeasurement(),"0");
                        }
                    });
                }
            }
        });
        updateThread.start();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_bluetooth) {
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            mDeviceListFragment = DeviceListFragment.newInstance(BTAdapter);
//            fragmentManager.beginTransaction().replace(R.id.container, mDeviceListFragment).commit();
            openMetrics();
        }

        return super.onOptionsItemSelected(item);
    }

    public static String getUsername() {
        return USERNAME;
    }

    public void testNotification(float dbValue) {

        long currentTime = System.currentTimeMillis();

        if (dbValue > RED_VALUE && !notificationSignaled && currentTime > nextNotificationTime) {
            showNotification();
            notificationSignaled = true;

            nextNotificationTime = currentTime + MIN_NOTIFICATION_INTERVAL;
        } else {
            notificationSignaled = false;
        }

        if (dbValue > RED_VALUE) {
            findViewById(R.id.headphoneWarningText).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.headphoneWarningText).setVisibility(View.INVISIBLE);
        }
    }

    private void showNotification(){
        // define sound URI, the sound to be played when there's a notification
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // intent triggered, you can add other intent for other actions
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
        // this is it, we'll build the notification!

        NotificationCompat.Builder  mNotification = new NotificationCompat.Builder(this)
                .setContentTitle("Noise Warning")    // set the title message to notification
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .setContentText("Dangerous dB levels detected; please wear your hearing protection");	  // set the descritption message

        // set the vibrate start time and end time
        long[] v = {0,3000};
        mNotification.setVibrate(v);

        // notification goes off when user click on tap
        mNotification.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(1, mNotification.build());
    }

    public void setColor(String dbValueText, TextView txt) {

        if (dbValueText != null) {
            txt.setText(dbValueText);
            float dbValue = Float.parseFloat(dbValueText);
            if (dbValue <= YELLOW_VALUE) {
                txt.setTextColor(Color.GREEN);
            } else if (dbValue > YELLOW_VALUE && dbValue <= RED_VALUE) {  /* else set the text color to red */
                txt.setTextColor(Color.YELLOW);
            } else {
                txt.setTextColor(Color.RED);

            }
        }
    }

    public void pushDashboardUpdate(String headphoneValue, String ambientValue) {

        headphoneTV = (TextView) findViewById(R.id.headphoneDBText);
        ambientTV = (TextView) findViewById(R.id.ambientDBText);
        if (headphoneValue != null && headphoneTV != null) {
            setColor(headphoneValue, headphoneTV);
           // Log.d("test", headphoneValue);
            testNotification(Float.parseFloat(headphoneValue));
        }


//        TODO Ambient values not yet implemented
        setColor(ambientValue, ambientTV);
        if (Float.parseFloat(ambientValue) > RED_VALUE) {
            findViewById(R.id.ambientWarningText).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ambientWarningText).setVisibility(View.INVISIBLE);
        }

    }



    @Override
    public void onFragmentInteraction(DeviceItem item) {
        Log.d("DEVICE", item.getDeviceName());
        currentBluetoothDevice = item;
        closeBluetoothList();
//        startBluetoothConnection(item);

    }

    public void openDash() {

        FragmentManager fragmentManager = getSupportFragmentManager();

        mDashboardFragment = DashboardFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.container, mDashboardFragment).commit();

//        SeekBar seekBar = (SeekBar) findViewById(R.id.offsetSlider);
//
//        if (seekBar != null) {
//            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//
//                @Override
//                public void onProgressChanged(SeekBar seekBar, int progress,
//                                              boolean fromUser) {
//                    // TODO Auto-generated method stub
//                    setOffset(progress);
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {
//
//                }
//
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//
//                }
//
//            });
//        }
//
//
//        SeekBar seekBar2 = (SeekBar) findViewById(R.id.multiplerslider);
//
//        if (seekBar2 != null) {
//            seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//
//                @Override
//                public void onProgressChanged(SeekBar seekBar, int progress,
//                                              boolean fromUser) {
//                    // TODO Auto-generated method stub
//                    setMultiplier(progress);
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {
//
//                }
//
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//
//                }
//
//            });
//        }
    }

    public void startBluetoothConnection(DeviceItem item) {

        if(bConnection != null) {
            bConnection.cancel();
        }

        if(item == null && defaultDevice != null) {

            //This statement allows BluetoothConnection to reset the device in case it disconnects.
            bConnection = new BluetoothConnection(defaultDevice.getDevice(), MainActivity.this);
            bConnection.start();
            setupClasses(bConnection);

        } else if (item != null){
            //This is the normal use case for this function
            bConnection = new BluetoothConnection(item.getDevice(), MainActivity.this);
            bConnection.start();
            setupClasses(bConnection);
        }
    }



    @Override
    public void closeBluetoothList() {
        openDash();
    }

    @Override
    public void dashboardFragment(Uri uri) {

    }

    public void bluetoothConnected() {
        TextView tv = (TextView) findViewById(R.id.connectedTV);

            if(tv != null) {
                tv.setVisibility(View.VISIBLE);

            }
    }

    public void bluetoothScoOn(boolean isBluetooth) {
        bluetoothActive = isBluetooth;
    }

    public void bluetoothDisconnected() {
        TextView tv = (TextView) findViewById(R.id.connectedTV);

        if (tv != null) {
            tv.setVisibility(View.INVISIBLE);
        }
    }

    public void setAverageAndPeak(double avg, double peak, boolean setPeak) {
        dailyAvg = avg;

        if (setPeak) {
            dailyPeak = peak;
        }


    }

    private void openMetrics() {

        new AlertDialog.Builder(this)
                .setTitle("Daily Metrics")
                .setMessage("Daily Average: " + df.format(dailyAvg) + "\nDaily Peak: " + df.format(dailyPeak))
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void disableRecordKeeping() {
        Log.d("BOUNDS", "USER HAS LEFT WORK LOCATION; DISABLE");
        Toast.makeText(this, "App disabled; Exited work area.", Toast.LENGTH_LONG).show();
        bluetoothDisconnected();
        isAppDisabled = true;
    }

    public void enableRecordKeeping() {
        Log.d("BOUNDS", "USER HAS ENTERED WORK LOCATION; ENABLE");
        Toast.makeText(this, "App enabled; Entered work area.", Toast.LENGTH_LONG).show();
        bluetoothConnected();
        isAppDisabled = false;
    }

    public boolean appDisabled() {
        //Log.d("BOUNDS", "Val in isAppDisabled: " + isAppDisabled);
        return isAppDisabled;
    }

//    public void setOffset(int offset) {
//        deviceMic.setOffset(offset/30.0);
//    }
//
//    public void setMultiplier(float multipler) {
//        deviceMic.setMultiplier((multipler/50.0);
//    }
}
