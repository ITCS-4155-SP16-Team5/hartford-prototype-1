package com.hartford.hartford_proto;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Jake on 2/21/2016.
 */
public class GeoLocator {

    private final Context context;
    private final MainActivity activity;
    private String latLocation;
    private String longLocation;
    private LocationManager locationManager;
    private boolean firstRun = true;
    private float accuracy;

    public GeoLocator(Context context, MainActivity activity){
        this.context = context;
        this.activity = activity;
        locationSetup();
    }

    public void locationSetup() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                accuracy = location.getAccuracy();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView lat  = (TextView) activity.findViewById(R.id.accuracy);
                        lat.setText(Float.toString(accuracy));


                    }

                });
                if(accuracy < 100) {


                    if (firstRun) {
                        activity.workLocationx = location.getLatitude();
                        activity.workLocationy = location.getLongitude();
                        Log.d("BOUNDS", "FIRST bounds location, set to mainActivity: " + activity.workLocationx + ", " + activity.workLocationy + ". Work size: " + activity.workSquare);

                        firstRun = false;
                    }
                    updateLocation(location);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

    }


    private void updateLocation(Location location) {

        latLocation = Double.toString(location.getLatitude());
        longLocation = Double.toString(location.getLongitude());

    }

    public String getLat() {

        if (latLocation!= null) {
            return latLocation;
        }
        return "0";
    }
    public String getLong() {

        if (longLocation!= null) {
            return longLocation;
        }
        return "0";
    }

    //check bounding. If all the information is good, and you are located outside of your work area, disable the app
    public void checkOutOfBounds() {
        NumberFormat df = new DecimalFormat("##.########");
        double lat = Double.parseDouble(getLat());
        double lng = Double.parseDouble(getLong());
//        Log.d("check",getLat() + ", " + getLong());
//        Log.d("check", (activity.workLocationy-activity.workSquare) + ", " + (activity.workLocationy+activity.workSquare));
     //   Log.d("check", "Difference in lng : " + df.format(activity.workLocationy - lng) + " Difference in lat: " + df.format(activity.workLocationx - lat));
        final double diffLat = activity.workLocationx - lat;
        final double diffLng = activity.workLocationy - lng;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                NumberFormat df = new DecimalFormat("##.########");
                TextView lat  = (TextView) activity.findViewById(R.id.diffLat);
                lat.setText(df.format(diffLat));
                TextView lng  = (TextView) activity.findViewById(R.id.diffLng);
                lng.setText(df.format(diffLng));
                TextView target = (TextView) activity.findViewById(R.id.target);
                target.setText(df.format(activity.workSquare));

            }

        });
        if (lat != 0 && lng != 0 && !firstRun && !activity.appDisabled() &&
                (  activity.workLocationy-activity.workSquare > lng
                || activity.workLocationy+activity.workSquare < lng
                || activity.workLocationx-activity.workSquare > lat
                || activity.workLocationx+activity.workSquare < lat)) {


                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activity.disableRecordKeeping();
                            }

            });



        }

    }

    public void checkInBounds() {
        //Log.d("check","In check in bounds");

        double lat = Double.parseDouble(getLat());
        double lng = Double.parseDouble(getLong());


//        Log.d("check",getLat() + ", " + getLong());
//        Log.d("check", (activity.workLocationy-activity.workSquare) + ", " + (activity.workLocationy+activity.workSquare));


        final double diffLat = activity.workLocationx - lat;
        final double diffLng = activity.workLocationy - lng;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                NumberFormat df = new DecimalFormat("##.########");
                TextView lat  = (TextView) activity.findViewById(R.id.diffLat);
                lat.setText(df.format(diffLat));
                TextView lng  = (TextView) activity.findViewById(R.id.diffLng);
                lng.setText(df.format(diffLng));
                TextView target = (TextView) activity.findViewById(R.id.target);
                target.setText(df.format(activity.workSquare));

            }

        });
        if (lat != 0 && lng != 0 && !firstRun &&
                (          activity.workLocationy-activity.workSquare < lng
                        && activity.workLocationy+activity.workSquare > lng
                        && activity.workLocationx-activity.workSquare < lat
                        && activity.workLocationx+activity.workSquare > lat)) {


            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.enableRecordKeeping();
                }

            });



        }


    }
}
