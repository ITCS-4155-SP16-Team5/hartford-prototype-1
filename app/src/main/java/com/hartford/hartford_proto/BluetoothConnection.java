package com.hartford.hartford_proto;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.view.View;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.text.DecimalFormat;
import java.util.UUID;

/**
 * NOTE: This class is fully deprecated! Use only if you wish to expand with custom bluetooth devices
 */


public class BluetoothConnection extends Thread{

    private final BluetoothDevice bTDevice;
    private  BluetoothSocket bTSocket;
    private boolean connectionEstablished = false;
    MainActivity activity;
    public String currentAmbientValue = "00";
    public String currentHeadphoneValue = "N/A";
    final int REFRESH_TIME = 50;
    long currentTime;
    long nextTimeout;
    final long TIMEOUT = 10000;
    private boolean firstRun = true;
    ObjectInputStream ois;
    DataInputStream dataInput;
    InputStream inputStream;
    SoundMeter sm;
    Microphone mic;
    DecimalFormat df;

    public BluetoothConnection(BluetoothDevice bTDevice, MainActivity activity) {

        this.bTDevice = bTDevice;
        this.activity = activity;
        df = new DecimalFormat("###.##");
    }

    public boolean connect() {

        BluetoothSocket tmp = null;
        try {
            tmp = bTDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
        } catch (IOException e) {
            Log.d("CONNECTIONTHREAD", " Error: " + e);
            e.printStackTrace();
        }
        bTSocket = tmp;
        try {
            bTSocket.connect();
            inputStream = bTSocket.getInputStream();
            dataInput = new DataInputStream(inputStream);
            connectionEstablished = true;

            if (connectionEstablished && firstRun) {
                enableBluetoothButton();
                Log.d("CONNECTIONTHREAD", "In first run check");
                firstRun = false;
                //activity.setupClasses(this);
                Log.d("CONNECTIONTHREAD", "after first run check");
            }
        } catch(IOException e) {
            Log.d("CONNECTIONTHREAD","Could not connect: " + e.toString());
            restartConnection();

        }
        return true;
    }

    @Override
    public void run() {
        super.run();
        connect();
        currentTime = System.currentTimeMillis();
        nextTimeout = currentTime + TIMEOUT;
        while (connectionEstablished) {

            Log.d("CONNECTIONTHREAD", "Before Connect");

            Log.d("CONNECTIONTHREAD", "In Loop");

                try {
                    Thread.sleep(REFRESH_TIME);
                    currentTime = System.currentTimeMillis();
                    if (dataInput.available() > 0) {
                        nextTimeout = currentTime + TIMEOUT;
                        byte[] msg = new byte[dataInput.available()];
                        dataInput.read(msg, 0, dataInput.available());

                        //TODO ultimately what we will use; temporarily using microphone data for test purposes
                        //setCurrentHeadphoneValue(new String(msg).substring(msg.length - 2, msg.length));

                        //setCurrentHeadphoneValue(String.valueOf(sm.getAmplitude()*100));


                        setCurrentHeadphoneValue(df.format(mic.readAudioBuffer()));

                        //TODO setCurrentAmbientValue
                        //Log.d ("AMP", String.valueOf(sm.getAmplitude()));

                        Log.d ("AMP", df.format(mic.readAudioBuffer()));
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activity.pushDashboardUpdate(currentHeadphoneValue, currentAmbientValue);
                            }
                        });
                    } else {
                        Log.d("CONNECTIONTHREAD", "data empty");
                    }
                    if (currentTime > nextTimeout) {
                        cancel();
                        restartConnection();
                    }
                } catch (InterruptedException e) {
                    Log.d("CONNECTIONTHREAD", "Error " + e);
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
                Log.d("CONNECTIONTHREAD", "no connection recorded established");

            }



    public boolean cancel() {

        try {
            connectionEstablished = false;
            Log.d("CONNECTIONTHREAD", "connection canceled1");
            disableBluetoothButton();
            Log.d("CONNECTIONTHREAD", "connection canceled2");
            bTSocket.close();
            Log.d("CONNECTIONTHREAD", "connection canceled3");
        } catch(IOException e) {
            return false;
        }
        return true;
    }



    public BluetoothSocket getSocket() {

        return bTSocket;
    }
    public boolean isConnected() {
        return connectionEstablished;
    }

    public void setCurrentHeadphoneValue(String currentHeadphoneValue) {
        this.currentHeadphoneValue = currentHeadphoneValue;
        Log.d("CONNECTIONTHREAD", "Value being set: " + currentHeadphoneValue);
    }

    public void setCurrentMicrophoneValue(String currentMicValue) {

        this.currentAmbientValue = currentMicValue;

    }

    public void setConnectionEstablished(boolean bool) {
        connectionEstablished = bool;
    }


    public void enableBluetoothButton() {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.bluetoothConnected();
            }
        });
    }

    public void disableBluetoothButton() {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.bluetoothDisconnected();
            }
        });
    }

    private void restartConnection() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.startBluetoothConnection(null);
            }
        });

    }
}
