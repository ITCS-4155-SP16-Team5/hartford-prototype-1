package com.hartford.hartford_proto;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jake on 2/21/2016.
 */
public class DataTransmissionThread extends Thread{

    private int transmissionInterval;
    private SQLiteHandler db;
    private HTTPSender http;
    private Context context;
    private MainActivity activity;
    public DataTransmissionThread(Context context, SQLiteHandler db, HTTPSender http, int transmissionInterval) {
        this.transmissionInterval = transmissionInterval;
        this.db = db;
        this.http = http;
        this.context = context;
        activity = (MainActivity) context;
    }

    @Override
    public void run() {
        while(true) {
            super.run();

            try {
                Thread.sleep(transmissionInterval);

                //add a Semaphore to the DB so that there is no conflict between saving, reading, and deleting from the DB
                if (!db.dbInUse() && isNetworkAvailable() && !activity.appDisabled()) {
                    db.dbInUse(true);
                    JSONArray jArray = db.getAllRecords();
                    jArray.put(new JSONObject().put("LAST_RECORD", db.getLastRecord()));
                  //  System.out.println("value of jArray immediately after retrieving from db: " + jArray.toString());
                    db.dbInUse(false);
                    http.sendData(jArray);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
