package com.hartford.hartford_proto;

import android.util.Log;

import java.io.IOException;

/**
 * Created by Jake on 2/21/2016.
 */
public class DataCollectionThread extends Thread {
    private int recordInterval;
    private SQLiteHandler db;
    private GeoLocator geo;
    private MainActivity activity;
    public DataCollectionThread(int interval, SQLiteHandler db, GeoLocator geo, MainActivity activity) {

        recordInterval = interval;
         this.db = db;
        this.activity = activity;
        this.geo = geo;
    }

    @Override
    public void run() {
        while(true) {
            super.run();
            try {
                Thread.sleep(recordInterval);

                if(!activity.appDisabled()) {
                    geo.checkOutOfBounds();
                } else {
                    geo.checkInBounds();
                }
                //add a Semaphore to the DB so that there is no conflict between saving, reading, and deleting from the DB
                if (!db.dbInUse() && !activity.appDisabled()) {
                    db.dbInUse(true);
                    saveDataToDB();
                    db.dbInUse(false);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void saveDataToDB() throws IOException {
        db.addRecordData();
    }


}
