package com.hartford.hartford_proto;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Created by Jake on 3/26/2016.
 */
public class ManageBluetoothConnection extends Thread {

    BluetoothConnection bt;
    public ManageBluetoothConnection(BluetoothConnection bt) {
        this.bt = bt;
        try {
            receiveData();
        } catch (IOException e) {
            bt.cancel();
            e.printStackTrace();
        }

    }

    public void sendData(BluetoothSocket socket, int data) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream(4);
        output.write(data);
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(output.toByteArray());
    }

    public int receiveData() throws IOException{
        //Log.d("RECEIVE", "In received data function");
        byte[] buffer = new byte[4];
        ByteArrayInputStream input = new ByteArrayInputStream(buffer);
        InputStream inputStream = bt.getSocket().getInputStream();
        InputStreamReader aReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(aReader);
        bt.setCurrentHeadphoneValue(bufferedReader.readLine());

        //inputStream.read(buffer);
        Log.d("RECEIVE", bufferedReader.readLine());
        return input.read();
    }
}