package com.hartford.hartford_proto;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jake on 2/21/2016.
 */
public class JSONPackager {

    private BluetoothReceiver bt;
    private HTTPSender http;
    private GeoLocator geo;
    private String username;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
    SharedPreferences.Editor editor;
    SharedPreferences settings;
    public static final String PREFS_NAME = "AOP_PREFS";

    public JSONPackager(BluetoothReceiver bt, HTTPSender http, GeoLocator geo, String username) {
        this.bt = bt;
        this.http = http;
        this.geo = geo;
        this.username = username;
    }


    public void packageAll() throws JSONException {
        JSONObject jObject = new JSONObject();
        jObject.put("TIMESTAMP", simpleDateFormat.format(new Date()));
        jObject.put("USERNAME", username);
        //TODO jObject.put("AMBIENT", bt.getAmbient());
        //TODO jObject.put("EAR", bt.getAmbient());
        //TODO jObject.put("COORDINATES", geo.getCoordinates());

    //Calendar.getInstance().get(Calendar.DATE)
    }
}
