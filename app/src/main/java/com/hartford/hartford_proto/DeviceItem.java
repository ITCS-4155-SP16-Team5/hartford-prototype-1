package com.hartford.hartford_proto;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

/**
 * Created by Jake on 3/26/2016.
 */
public class DeviceItem {

    private String deviceName;
    private String address;
    private boolean connected;
    private BluetoothDevice device;

    public String getDeviceName() {
        return deviceName;
    }

    public boolean getConnected() {
        return connected;
    }

    public String getAddress() {
        return address;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public DeviceItem(String name, String address, String connected, BluetoothDevice device){
        this.deviceName = name;
        this.address = address;
        this.device = device;
        if (connected == "true") {
            this.connected = true;
        }
        else {
            this.connected = false;
        }
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}