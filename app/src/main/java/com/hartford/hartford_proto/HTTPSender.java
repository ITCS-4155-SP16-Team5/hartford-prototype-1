package com.hartford.hartford_proto;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Jake on 2/21/2016.
 */
public class HTTPSender {

    private SQLiteHandler DB;

    public HTTPSender() {

    }


    public void sendData(JSONArray data) {
        if (data.length() > 0) {
            new SendDataToServer().execute(data.toString());
        }
    }

    public void setDB(SQLiteHandler DB) {
        this.DB = DB;
    }


    class SendDataToServer extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String JsonResponse = null;
            String JsonDATA = params[0];
           // System.out.println("Value in JsonDATA: " + JsonDATA);

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {

                //URL of the server you want to send to
                URL url = new URL("http://httpbin.org/post");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                //set headers and method
                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                writer.write(JsonDATA);
                // json data
                writer.close();
                InputStream inputStream = urlConnection.getInputStream();
                //input stream
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String inputLine;
                while ((inputLine = reader.readLine()) != null)
                    buffer.append(inputLine + "\n");
                if (buffer.length() == 0) {
                    // Stream was empty. No point in parsing.
                    return null;
                }
                JsonResponse = buffer.toString();


//response data
                Log.i("httpReturn", JsonResponse);
                //send to post execute
                String response = parseResponse(JsonResponse);
                //System.out.println("value of reponse: " + response);
                DB.deleteSelection(response);
               // System.out.println("after db delete");
                return "SUCCESS";


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("http", "Error closing stream", e);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
        }

    }

    private String parseResponse(String jsonObject) throws JSONException {

        JSONObject jObject = new JSONObject(jsonObject);
        //strip response down to the data portion and convert to an array
        JSONArray jArray = new JSONArray(jObject.get("data").toString());
        //strip off last element and get ID of last record; this will be used to determine which elements from the database should be erased
        String lastRecord = new JSONObject(jArray.get(jArray.length()-1).toString()).getString("LAST_RECORD");
        System.out.println("LAST RECORD IN DB: " + lastRecord);

        return lastRecord;
    }
}
