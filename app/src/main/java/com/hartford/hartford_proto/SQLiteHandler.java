package com.hartford.hartford_proto;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jake on 2/21/2016.
 */
public class SQLiteHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_METRICS = "metrics";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_AMBIENT = "ambient";
    private static final String KEY_EAR = "ear";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_COORDINATES = "coordinates";


    private BluetoothConnection bt;
    private HTTPSender http;
    private GeoLocator geo;
    private Context context;
    public Microphone mic;


    private boolean inUse = false;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
    private boolean lastRecord;


    public SQLiteHandler(Context context, BluetoothConnection bt, HTTPSender http, GeoLocator geo, Microphone mic) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

            this.bt = bt;
            this.http = http;
            this.geo = geo;
            this.context = context;
        this.mic = mic;

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_METRICS + "("
                + KEY_AMBIENT + " TEXT," + KEY_EAR  + " TEXT," + KEY_USERNAME + " TEXT," + KEY_TIMESTAMP + " TEXT,"
                + KEY_COORDINATES + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_METRICS);

        // Create tables again
        onCreate(db);
    }

    // Adding new contact
    public void addRecordData() throws IOException {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        //for testing only
        values.put(KEY_AMBIENT, "AMBIENT_SOUND_FROM_BLUETOOTH"); // Contact Name
        values.put(KEY_EAR, "EAR_SOUND"); // Contact Phone Number
        values.put(KEY_USERNAME, MainActivity.getUsername());
        values.put(KEY_TIMESTAMP, simpleDateFormat.format(new Date()));
        values.put(KEY_COORDINATES, geo.getLat() + " " + geo.getLong());

        //values.put(KEY_AMBIENT, String.valueOf(bt.currentAmbientValue));
        //values.put(KEY_EAR, String.valueOf(bt.currentHeadphoneValue));

        values.put(KEY_EAR, mic.readAudioBuffer());

        values.put(KEY_AMBIENT, "0.00");
        // Inserting Row

        db.insert(TABLE_METRICS, null, values);

        db.close(); // Closing database connection
        Log.d("DB", "RECORD SAVED");
    }

    public JSONArray getAllRecords() throws JSONException {
        JSONArray recordList = new JSONArray();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_METRICS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject jObject = new JSONObject();

                jObject.put(KEY_AMBIENT, cursor.getString(0)); // Contact Name
                jObject.put(KEY_EAR, cursor.getString(1)); // Contact Phone Number
                jObject.put(KEY_USERNAME, cursor.getString(2)); // Contact Phone Number
                jObject.put(KEY_TIMESTAMP, cursor.getString(3)); // Contact Phone Number
                jObject.put(KEY_COORDINATES, cursor.getString(4)); // Contact Phone Number

                // Adding JSONObject to array
                recordList.put(jObject);
            } while (cursor.moveToNext());
        }

//        //This try is necessary so that if the delete fails, the information is still sent
//        try {
//          //  deleteAllRecords();
//        } catch (Exception e) {
//            //Throw an error to the user or Hartford if delete fails for some reason
//        }
        // return contact list
        return recordList;
    }

    private void deleteAllRecords() {
        SQLiteDatabase db = this.getWritableDatabase();
        //to delete all records, simply drop the table and remake it
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_METRICS);
        // Create tables again
        onCreate(db);


    }

    public void dbInUse(boolean inUse) {
        this.inUse = inUse;
    }

    public boolean dbInUse() {
        return inUse;
    }


    //This function only deletes the records up to the point that they've been received downstream (Hartford)
    public void deleteSelection(String endID) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String startID = getLastRecord();

            if (startID != null && endID != null) {
                // String deleteQuery = "delete from " + TABLE_METRICS + " where ROWID < " + startID + " and ROWID < " + endID;
                db.delete(TABLE_METRICS, "ROWID <= " + endID, null);
            }

        } catch (Exception e) {

        }
    }

    public String getLastRecord() {
        String startID = null;
        String selectQuery = "SELECT ROWID FROM " +  TABLE_METRICS + " ORDER BY ROWID DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            startID = (Long.toString(cursor.getLong(0)));
        }
            return startID;
    }
}
